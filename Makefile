SRCDIR=./src
INCLUDEDIR=./include
OBJDIR=./obj
EXT=

CPP=g++
CFLAGS=-Wall -Wextra -pedantic -std=c++17 -fno-common -fno-builtin `pkg-config sfml-graphics --cflags` -I$(INCLUDEDIR)
LDFLAGS=`pkg-config sfml-graphics --libs` `pkg-config glew --libs`
SRC= $(wildcard $(SRCDIR)/*.cpp)
HEADERS= $(wildcard $(INCLUDEDIR)/*.hpp)

# $@ est le nom de la cible
# $^ est la liste des dépendances
# $< est la première dépendance

# rajouter @ devant une commande pour la rendre silencieuse

ifeq ($(DEBUG),y)
	CFLAGS += -DDEBUG -g3 -O0 -Wall -Wextra -pedantic
	EXT=-d
else
	CFLAGS += -O3 -DNDEBUG
endif

EXEC=sfmlgl$(EXT)
OBJ=$(SRC:$(SRCDIR)/%.cpp=$(OBJDIR)/%$(EXT).o)

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CPP) -o $@ $^ $(LDFLAGS)

# Construction automatique des .o à partir des .cpp
$(OBJDIR)/%$(EXT).o:$(SRCDIR)/%.cpp $(HEADERS)
	$(CPP) -o $@ -c $< $(CFLAGS)

# .PHONY est la cible qui est systématiquement reconstruite (utile si il existe déjà des fichiers clean ou mrproper dans le répertoire courant, il serait alors forcément plus récent que ses dépendances et la règle ne serait alors jamais exécutée.
.PHONY: clean rebuild

clean:
	rm -rf $(OBJ)
	rm -rf $(EXEC)

rebuild:
	$(MAKE) clean
	$(MAKE) all
