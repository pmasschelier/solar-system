#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "Input.hpp"

class Camera
{
public:

    Camera(glm::vec3 const& position = glm::vec3(0, 0, 0), 
        float horizontalAngle = 0.f,
        float verticalAngle = 0.f,
        float FoV = 45.f,
        float speed = 3.f,
        float mouseSpeed = 0.05f,
        float ratio = 4.0f / 3.0f,
        float nearClipping = 0.1f, 
        float farClipping = 100.0f);

    glm::mat4 update(Input const& in, sf::Time const& elapsedTime);

    glm::mat4 setPosition(glm::vec3 position);
    glm::vec3 getPosition() const;

    glm::mat4 move(glm::vec3 displacement);

    glm::mat4 setTarget(glm::vec3 target);

    glm::mat4 setDirection(glm::vec3 direction);
    glm::vec3 getDirection() const;

    glm::vec3 getRight() const;
    glm::vec3 getUp() const;
    
    glm::mat4 setFov(float FoV);
    float getFov() const;

    void setViewport(float x, float y, float w, float h);

    void setSpeed(float speed);
    float getSpeed() const;

    void setMouseSpeed(float mouseSpeed);
    float getMouseSpeed() const;

    glm::mat4 setRatio(float ratio);
    float getRatio() const;

    glm::mat4 setNearClipping(float ratio);
    float getNearClipping() const;

    glm::mat4 setFarClipping(float ratio);
    float getFarClipping() const;

    glm::mat4 getViewMatrix() const;
    glm::mat4 getProjectionMatrix() const;
    glm::mat4 getViewProjectionMatrix() const;

private:

    glm::mat4 updateProjection();
    glm::mat4 updateView();
    void updateDirectionRightUp();

    glm::vec3 m_position;
    glm::vec3 m_direction;
    glm::vec3 m_right;
    glm::vec3 m_up;
    // angle horizontal = 0.f -> regard vers +z
    float m_horizontalAngle = 3.14f;
    // angle vertical = 0.f -> regard vers l'horizon
    float m_verticalAngle;
    float m_FOV;

    float m_speed; // unités/s
    float m_mouseSpeed; // = 0.005f

    float m_ratio;
    float m_nearClipping, m_farClipping;

    glm::mat4 m_projection, m_view, m_viewProjection;
};

#endif