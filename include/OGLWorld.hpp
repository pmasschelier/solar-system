#ifndef OGLWORLD_HPP
#define OGLWORLD_HPP

#include <SFML/Graphics.hpp>
#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <tuple>
#include <array>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <cassert>

#ifndef BUFFER_OFFSET
#define BUFFER_OFFSET(offset) ((char*)NULL + (offset))
#endif

#endif
