#ifndef SCENE_HPP
#define SCENE_HPP

#include "OGLWorld.hpp"
#include "Camera.hpp"
#include "Shader.hpp"
#include "Cube.hpp"
#include "Skybox.hpp"
#include "Sphere.hpp"

class OGLScene {
public:
	OGLScene(std::shared_ptr<sf::RenderWindow> window);
	~OGLScene();
	void play();

private:

	void loadModels();
	void draw() const;
	
private:
	sf::ContextSettings settings;
	std::shared_ptr<sf::RenderWindow> m_window;

	std::vector<float> sphere_vertices;
	std::vector<unsigned> sphere_elements;
	std::vector<float> sphere_uvs;
	GLuint m_sphereVBO, m_sphereUVsBO, m_sphereEBO, m_sphereVAO;

	sf::Texture m_sunTex, m_earthTex;
	glm::mat4 m_sunModel, m_earthModel;
	Shader m_sunShader, m_earthShader;

	Light m_light;

	Camera m_camera;

	Skybox m_skybox;
	std::shared_ptr<CubemapTexture> m_skytexture;
};

#endif
