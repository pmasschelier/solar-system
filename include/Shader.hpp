#ifndef SHADER_HPP
#define SHADER_HPP

#include "OGLWorld.hpp"
#include "Light.hpp"

class Shader {
private:
    struct shader_t{
        std::string filename;
        GLuint id;
        GLenum type;
    };

public:
	Shader();
    ~Shader();

    /* \brief Ajoute un shader au pipeline
     * Charge le fichier source et compile avant de l'ajouter à la liste des shaders
     * \param filename Chemin vers le fichier à charger
     * \param shaderType GL_VERTEX_SHADER, GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER, GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER, GL_COMPUTE_SHADER
     */
    void addFile(std::string const& filename, GLenum shaderType);

    /* \brief Ajoute un shader au pipeline
     * Compile le code source avant de l'ajouter à la liste des shaders
     * \param filename Code source
     * \param shaderType GL_VERTEX_SHADER, GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER, GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER, GL_COMPUTE_SHADER
     * \param name nom du shader (facultatif, uniquement à des fins de débogage)
     */
    void addSource(std::string const& source, GLenum shaderType, std::string const& name = "");

    /* \brief Link le shader
     * Seulement après l'appel de cette fonction le shader est prêt à être utilisé
     */
    void load();

    /* \brief Retourne l'id du programme
     * L'id est celui renvoyé par glCreateProgram()
     * \return id du programme
     */
    GLuint getID() const {return m_id;}

    /* \brief Renvoie m_ready
     * Renvoie true ssi load() a été appelée
     */
    bool ready() const;

    void setBool(const std::string &name, bool value) const;  
    void setInt(const std::string &name, int value) const;   
    void setFloat(const std::string &name, float value) const;
    void setVec3(const std::string &name, glm::vec3 vec) const;
    void setMat4(const std::string &name, glm::mat4 mat) const;
    void setLight(const std::string &name, Light const& light) const;

private:

    static GLuint compile(GLenum shaderType, std::string const& source);
	
private:
    std::vector<shader_t> m_shaders;

	GLuint m_id;
    bool m_ready;
};

#endif
