#version 330 core
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uvs;

uniform mat4 viewProjectionMatrix;
uniform mat4 model;

out vec2 texCoord;

void main() {
    gl_Position = viewProjectionMatrix * model * vec4(position, 1.0); // w = 1 -> position, w = 0 -> direction
    texCoord = uvs;
}