#include "CubemapTexture.hpp"

CubemapTexture::CubemapTexture() : m_smooth(true), m_hasMipmap(false)
{
    
}

CubemapTexture::~CubemapTexture()
{
    if(glIsTexture(m_id) == GL_TRUE)
        glDeleteTextures(1, &m_id);
}

void CubemapTexture::create()
{
    if(glIsTexture(m_id) == GL_TRUE)
        glDeleteTextures(1, &m_id);

    glGenTextures(1, &m_id);
}

/* 0x8515	GL_TEXTURE_CUBE_MAP_POSITIVE_X
 * 0x8516	GL_TEXTURE_CUBE_MAP_NEGATIVE_X
 * 0x8517	GL_TEXTURE_CUBE_MAP_POSITIVE_Y
 * 0x8518	GL_TEXTURE_CUBE_MAP_NEGATIVE_Y
 * 0x8519	GL_TEXTURE_CUBE_MAP_POSITIVE_Z
 * 0x851A	GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
 */

bool CubemapTexture::loadFromFiles(std::array<std::string, 6> const& filenames)
{
    create();

    glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);

    for (unsigned int i = 0 ; i < filenames.size() ; i++) {
        
        sf::Image image;

        if(!image.loadFromFile(filenames[i]))
        {
            std::cerr << "Erreur lors du chargement de : " << filenames[i] << std::endl;
            return false;
        }

        /* target : GL_TEXTURE_2D, GL_PROXY_TEXTURE_2D, GL_TEXTURE_1D_ARRAY, GL_PROXY_TEXTURE_1D_ARRAY, GL_TEXTURE_RECTANGLE, GL_PROXY_TEXTURE_RECTANGLE, GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X, GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, GL_PROXY_TEXTURE_CUBE_MAP
         * level : n indique que la texture est la n-ème mipmap-reduction de l'image. si target = GL_TEXTURE_RECTANGLE ou GL_PROXY_TEXTURE_RECTANGLE alors level = 0
         * internalFormat : GL_DEPTH_COMPONENT, GL_DEPTH_STENCIL, GL_STENCIL_INDEX, GL_RED, GL_RG, GL_RGB, GL_RGBA (il existe aussi des formats compressés)
         * width : largeur de la texture. Les textures sont supportés au moins jusqu'à 1024 texels de large.
         * height : hauteur de la texture ou nombre de layers pour un tableau de textures si target = GL_TEXTURE_1D_ARRAY ou GL_PROXY_TEXTURE_1D_ARRAY. Les textures sont supportés au moins jusqu'à 1024 texels de haut et les tableaux supportent au moins 256 layers.
         * border : Toujours égal à 0.
         * format : format des donnés d'entrée (cf. internalFormat). Pour transférer de depth, stencil, ou depth/stencil data on utilise GL_DEPTH_COMPONENT, GL_STENCIL_INDEX, ou GL_DEPTH_STENCIL. Pour transférer d'entiers normalisés ou d'images en float on utilise GL_RED, GL_GREEN, GL_BLUE, GL_RG, GL_RGB, GL_BGR, GL_RGBA, et GL_BGRA. Pour des entiers non-normalisés on utilise GL_RED_INTEGER, GL_GREEN_INTEGER, GL_BLUE_INTEGER, GL_RG_INTEGER, GL_RGB_INTEGER, GL_BGR_INTEGER, GL_RGBA_INTEGER, et GL_BGRA_INTEGER.
         * type : GL_UNSIGNED_BYTE, GL_BYTE, GL_UNSIGNED_SHORT, GL_SHORT, GL_UNSIGNED_INT, GL_INT, GL_FLOAT, GL_UNSIGNED_BYTE_3_3_2, GL_UNSIGNED_BYTE_2_3_3_REV, GL_UNSIGNED_SHORT_5_6_5, GL_UNSIGNED_SHORT_5_6_5_REV, GL_UNSIGNED_SHORT_4_4_4_4, GL_UNSIGNED_SHORT_4_4_4_4_REV, GL_UNSIGNED_SHORT_5_5_5_1, GL_UNSIGNED_SHORT_1_5_5_5_REV, GL_UNSIGNED_INT_8_8_8_8, GL_UNSIGNED_INT_8_8_8_8_REV, GL_UNSIGNED_INT_10_10_10_2, et GL_UNSIGNED_INT_2_10_10_10_REV.
         * data : Pointeur vers les données en mémoire, ou si le buffer est lié à GL_PIXEL_UNPACK_BUFFER, ce paramètre donne un offset dans le buffer. Si le buffer n'est pas lié à GL_PIXEL_UNPACK_BUFFER, et que ce paramètre vaut NULL, aucun transfert de pixel n'est effectué.
         */
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, image.getSize().x, image.getSize().y, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.getPixelsPtr());
    }
    
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);


    return true;
}

bool CubemapTexture::loadFromTileMap(unsigned widthTex, unsigned heightTex, std::array<unsigned, 6> const& index, std::string const& filename)
{
    sf::Image image, tile;

    if(!image.loadFromFile(filename))
    {
        std::cerr << "Erreur lors du chargement de : " << filename << std::endl;
        return false;
    }

    unsigned uX(image.getSize().x/widthTex), 
        uY(image.getSize().y/heightTex);
    
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);

    for (unsigned int i = 0 ; i < filename.size() ; i++) {
        unsigned posXonTex = index[i] % widthTex;
        unsigned posYonTex = index[i] / widthTex;

        tile.copy(image, 0, 0, sf::IntRect(posXonTex*uX, posYonTex*uY, uX, uY), true);

        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, tile.getSize().x, tile.getSize().y, 0, GL_RGBA, GL_UNSIGNED_BYTE, tile.getPixelsPtr());
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    return true;
}

/* GL_NEAREST
    * Retourne le pixel le plus proche (distance de Manhattan) de la coordonnée spécifiée.
    * GL_LINEAR
    * Retourne la moyenne pondérée des quatre pixels les plus proches de la coordonnée spécifiée.
    * GL_NEAREST_MIPMAP_NEAREST
    * Comme GL_NEAREST mais avec le mipmap le plus proche de la taille du pixel
    * GL_LINEAR_MIPMAP_NEAREST
    * Comme GL_LINEAR mais avec le mipmap le plus proche de la taille du pixel
    * GL_NEAREST_MIPMAP_LINEAR
    * Comme GL_NEAREST sur chacune des mipmap les plus proches de la taille du pixel. Le résultat est une moyenne pondérée de ces deux valeurs.
    * GL_LINEAR_MIPMAP_LINEAR
    * Comme GL_LINEAR sur chacune des mipmap les plus proches de la taille du pixel. Le résultat est une moyenne pondérée de ces deux valeurs.
*/

void CubemapTexture::setSmooth(bool smooth)
{
    if(smooth != m_smooth && glIsTexture(m_id) == GL_TRUE) {
        glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);
        m_smooth = smooth;

        if(m_smooth) {
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            if(m_hasMipmap)
                glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            else
                glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        }
        else {
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            if(m_hasMipmap)
                glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
            else
                glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        }

    }
}

bool CubemapTexture::isSmooth() const {
    return m_smooth;
}

void CubemapTexture::generateMipmap() {
    if(!m_hasMipmap && glIsTexture(m_id) == GL_TRUE) {
        m_hasMipmap = true;

        glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);
        glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
        if(m_smooth)
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        else
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
    }
    
}

void CubemapTexture::bind(GLenum texUnit)
{
    glActiveTexture(texUnit); // Numéro de la texture dans le fragment shader GL_TEXTURE0...GL_TEXTURE15
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);
}