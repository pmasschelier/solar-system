#include "Scene.hpp"


OGLScene::OGLScene(std::shared_ptr<sf::RenderWindow> window) : 
	m_window(window),
	m_light(glm::vec3(0, 2, 0)),
	m_camera(
		glm::vec3(-24, 18, -18), // Position de la caméra
		M_PI/4, // horizontalAngle
		-M_PI/5, // verticalAngle
		45.f // FoV
	),
	m_skytexture(std::make_shared<CubemapTexture>())
{
	m_camera.setRatio((float)m_window->getSize().x / (float) m_window->getSize().y);

	m_window->setActive(true);

	glEnable(GL_DEPTH_TEST);
	// Accepte le fragment s'il est plus proche de la caméra que l'ancien
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE); // Par défaut : glCullFace(GL_CCW);
	
	loadModels();
}

OGLScene::~OGLScene()
{
	glDeleteVertexArrays(1, &m_sphereVAO);
}

void OGLScene::loadModels() 
{
	// VAO de la sphere
	
    Sphere::computeVertices(100, 100, sphere_vertices, sphere_elements, sphere_uvs);

	// Sommets
	if(glIsBuffer(m_sphereVBO) == GL_TRUE)
        glDeleteBuffers(1, &m_sphereVBO);
    glGenBuffers(1, &m_sphereVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_sphereVBO);
	glBufferData(GL_ARRAY_BUFFER, sphere_vertices.size() * sizeof(float), &sphere_vertices[0], GL_STATIC_DRAW);

	// Coordonnées de texture
	if(glIsBuffer(m_sphereUVsBO) == GL_TRUE)
        glDeleteBuffers(1, &m_sphereUVsBO);
    glGenBuffers(1, &m_sphereUVsBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_sphereUVsBO);
	glBufferData(GL_ARRAY_BUFFER, sphere_uvs.size() * sizeof(float), &sphere_uvs[0], GL_STATIC_DRAW);

	// Éléments
	if(glIsBuffer(m_sphereEBO) == GL_TRUE)
        glDeleteBuffers(1, &m_sphereEBO);
    glGenBuffers(1, &m_sphereEBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_sphereEBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sphere_elements.size() * sizeof(unsigned int), &sphere_elements[0], GL_STATIC_DRAW);

	if(glIsVertexArray(m_sphereVAO) == GL_TRUE)
		glDeleteVertexArrays(1, &m_sphereVAO);
	glGenVertexArrays(1, &m_sphereVAO);

	glBindVertexArray(m_sphereVAO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_sphereEBO);

		glBindBuffer(GL_ARRAY_BUFFER, m_sphereVBO);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, m_sphereUVsBO);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(1);

	glBindVertexArray(0);

	m_sunModel = glm::scale(glm::vec3(5.f, 5.f, 5.f));
	m_earthModel = glm::translate(glm::vec3(10.f, 0.f, 0.f));
}

void OGLScene::draw() const
{
	// Dessine la sphère

	glUseProgram(m_sunShader.getID());
	
	glBindVertexArray(m_sphereVAO);

		m_sunShader.setMat4("viewProjectionMatrix", m_camera.getViewProjectionMatrix());
		m_sunShader.setMat4("model", m_sunModel);
		m_sunShader.setVec3("cameraPos", m_camera.getPosition());

		m_sunShader.setLight("light", m_light);

		sf::Texture::bind(&m_sunTex);

		glDrawElements(GL_TRIANGLES, sphere_elements.size(), GL_UNSIGNED_INT, 0); // Commence au sommet 0; 6 faces au total -> 12 triangles*

		sf::Texture::bind(nullptr);

	glBindVertexArray(0);

	glBindVertexArray(m_sphereVAO);

		m_earthShader.setMat4("viewProjectionMatrix", m_camera.getViewProjectionMatrix());
		m_earthShader.setMat4("model", m_earthModel);
		m_earthShader.setVec3("cameraPos", m_camera.getPosition());

		m_earthShader.setLight("light", m_light);

		sf::Texture::bind(&m_earthTex);

		glDrawElements(GL_TRIANGLES, sphere_elements.size(), GL_UNSIGNED_INT, 0); // Commence au sommet 0; 6 faces au total -> 12 triangles*

		sf::Texture::bind(nullptr);

	glBindVertexArray(0);

	m_skybox.render(m_camera);
}

void OGLScene::play()
{
    Input in(m_window, true);

	std::string shadersDir("ressources/shaders/");

	auto loadTex = [] (sf::Texture& tex, std::string const& filename) {
		sf::Image img;
		img.loadFromFile(filename);
		img.flipVertically();
		tex.loadFromImage(sunImg);
		tex.setSmooth(true);
		tex.generateMipmap();
	};

	loadTex(m_sunTex, "ressources/textures/2k_sun.jpg");
	m_sunShader.addFile(shadersDir + "sphere.vert", GL_VERTEX_SHADER);
	m_sunShader.addFile(shadersDir + "sun.frag", GL_FRAGMENT_SHADER);
	m_sunShader.load();
	
	loadTex(m_earthTex, "ressources/textures/2k_earth_daymap.jpg");
	m_earthShader.addFile(shadersDir + "sphere.vert", GL_VERTEX_SHADER);
	m_earthShader.addFile(shadersDir + "sun.frag", GL_FRAGMENT_SHADER);
	m_earthShader.load();

	std::array<std::string, 6> filenames = {
		"ressources/skybox/skybox_px.jpg",
		"ressources/skybox/skybox_nx.jpg",
		"ressources/skybox/skybox_py.jpg",
		"ressources/skybox/skybox_ny.jpg",
		"ressources/skybox/skybox_pz.jpg",
		"ressources/skybox/skybox_nz.jpg",
	};
	m_skytexture->loadFromFiles(filenames);
	m_skybox.setCubemapTexture(m_skytexture);

	bool wireframe(false), paused(false);
	sf::Clock loopclock;
	sf::Time lastTime(loopclock.getElapsedTime()), newTime, elapsedTime;

    while (!in.end())
    {
        in.update();

		if(in.appSizeChanged()) {
			float xw = (float)in.appSize().x, yw = (float) in.appSize().y;
			m_camera.setViewport(0.f, 0.f, xw, yw);
			m_window->setView(sf::View(sf::FloatRect(0.f, 0.f, xw, yw)));
		}

		if(in.getKey(sf::Keyboard::W)) {
			if(!wireframe)
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			else
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			wireframe = !wireframe;
			in.setKey(sf::Keyboard::W, false);
		}

		if(in.getKey(sf::Keyboard::P)) {
			in.setRelativeMouse(paused);
			paused = !paused;
			in.setKey(sf::Keyboard::P, false);
		}

		if(!paused) {
			newTime = loopclock.getElapsedTime();
			elapsedTime = newTime - lastTime;

			m_camera.update(in, elapsedTime);
			m_light.setPosition({5 * std::cos(M_PI * 0.1 * newTime.asSeconds()), 2, 5 * std::sin(M_PI * 0.1 * newTime.asSeconds())});
			m_earthModel = glm::rotate((float) (2.f * M_PI * newTime.asSeconds() / 120.f), glm::vec3(0.f, 1.f, 0.f)) 
				* glm::translate(glm::vec3(10.f, 0.f, 0.f)) 
				* glm::rotate((float) (2.f * M_PI * newTime.asSeconds() / 10.f), glm::vec3(0.f, 1.f, 0.f));

			lastTime = newTime;
		}
        
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		draw();

		m_window->display();

        sf::sleep(sf::milliseconds(25));
    }
}
