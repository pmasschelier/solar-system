#include "Sphere.hpp"

void Sphere::computeVertices(
    unsigned short meridians,
    unsigned short parallels,
    std::vector<float>& vertices,
    std::vector<unsigned int>& elements,
    std::vector<float>& uvs)
{
    if(parallels < 1)
        throw std::runtime_error("Pour une sphère le nombre de paralleles doit être supérieur à 1.");
    
    const unsigned int nb_vertices = 3*2 + 3*parallels*(meridians+1);
    const unsigned int nb_elements = 6*parallels*(meridians+1);
    const unsigned int nb_uvs = 2*2 + 2*parallels*(meridians+1);

    uvs.resize(nb_uvs);
    vertices.resize(nb_vertices);
    elements.resize(nb_elements);

    unsigned int vindex(0), eindex(0), uvindex(0);

#ifdef DEBUG
    std::cout << "j\tradius\t\tx\ty\tz\tuvx\tuvy" << std::endl;
#endif

    /* @brief Ajoute un point sur un parallèle particulier
     * @param j numéro du méridien
     * @param radius rayon du parallèle
     * @param y hauteur du parallèle
     */
    auto addPoint = [meridians, parallels, &vindex, &vertices, &uvindex, &uvs] (unsigned j, float radius, float y, float uvy) {
        float uvx = (float)j / meridians;
        float phi = 2 * M_PI * uvx;
        float x = std::sin(phi) * radius;
        float z = std::cos(phi) * radius;
        
        vertices[vindex++] = x;
        vertices[vindex++] = y;
        vertices[vindex++] = z;

#ifdef DEBUG
        std::cout << j << '\t' << radius << "\t" << x << "\t" << y << "\t" << z << "\t" << uvx << "\t" << uvy << std::endl;
#endif

        uvs[uvindex++] = uvx;
        uvs[uvindex++] = uvy;
    };

    // Point haut (addPoint(0, ))
    vertices[vindex++] = 0.0;
    vertices[vindex++] = 1.0;
    vertices[vindex++] = 0.0;

    uvs[uvindex++] = 0.5;
    uvs[uvindex++] = 1.0;

    // Indices du haut au premier parallèle
    unsigned i(0);
    for(; i + 1 < meridians; i++) {
        elements[eindex++] = 0;
        elements[eindex++] = i+1;
        elements[eindex++] = i+2;
    }

    elements[eindex++] = 0;
    elements[eindex++] = i+1;
    elements[eindex++] = i+2;

    double theta;
    float y, radius; // hauteur et rayon de chaque section de parallele
    unsigned offset; // Premier indice de chaque paralleleS
    float uvy; // uvs coordinates

    for(unsigned i(0); i < parallels; i++) {
        theta = M_PI * (i + 1) / (parallels + 1);
        y = std::cos(theta);
        radius = std::sin(theta);
        uvy = (1.f + y) / 2.f; 

        offset = 1 + i * (meridians + 1);

        for(unsigned j(0); j < meridians; j++) {
            // On crée les points
            addPoint(j, radius, y, uvy);

            if(j + 1 == meridians)
                addPoint(j+1, radius, y, uvy);

            // On crée les triangles
            if(i + 1 == parallels) { // Si on est sur le dernier parallèle
                elements[eindex++] = offset + j;
                elements[eindex++] = offset + meridians + 1;
                elements[eindex++] = offset + j + 1;
            }
            else {
                elements[eindex++] = offset + j;
                elements[eindex++] = offset + j + meridians + 1;
                elements[eindex++] = offset + j + 1;
                elements[eindex++] = offset + j + 1;
                elements[eindex++] = offset + j + meridians + 1;
                elements[eindex++] = offset + j + meridians + 2;
            }
        }
    }

    // Point bas
    vertices[vindex++] = 0.0;
    vertices[vindex++] = -1.0;
    vertices[vindex++] = 0.0;

    uvs[uvindex++] = 0.5;
    uvs[uvindex++] = 0.0;

    assert(vindex == vertices.size());
    assert(uvindex == uvs.size());
    assert(eindex == elements.size());
}

